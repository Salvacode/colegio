﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interfaz_Grafica
{
    public partial class RetirarAlumno : Form
    {
        public RetirarAlumno()
        {
            InitializeComponent();
        }

        private void tmrMovimiento_Tick(object sender, EventArgs e)
        {
            if (btnRetirar.Location.X == 532)
                btnRetirar.Location = new Point(495, 98);
            else if (btnRetirar.Location.X == 495)
                btnRetirar.Location = new Point(532, 98);
        }

        private void IconClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
