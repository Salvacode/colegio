﻿namespace Interfaz_Grafica
{
    partial class contraseña
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtncontraseña = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtccontraseña = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.epmal = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.epmal)).BeginInit();
            this.SuspendLayout();
            // 
            // txtncontraseña
            // 
            this.txtncontraseña.Location = new System.Drawing.Point(51, 43);
            this.txtncontraseña.Multiline = true;
            this.txtncontraseña.Name = "txtncontraseña";
            this.txtncontraseña.Size = new System.Drawing.Size(289, 26);
            this.txtncontraseña.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(52, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nueva contraseña";
            // 
            // txtccontraseña
            // 
            this.txtccontraseña.Location = new System.Drawing.Point(51, 97);
            this.txtccontraseña.Multiline = true;
            this.txtccontraseña.Name = "txtccontraseña";
            this.txtccontraseña.Size = new System.Drawing.Size(289, 26);
            this.txtccontraseña.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(52, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Confirme contraseña";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(281, 140);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Cambiar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // epmal
            // 
            this.epmal.ContainerControl = this;
            // 
            // contraseña
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(415, 186);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtccontraseña);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtncontraseña);
            this.Name = "contraseña";
            this.Text = "contraseña";
            this.Load += new System.EventHandler(this.contraseña_Load);
            ((System.ComponentModel.ISupportInitialize)(this.epmal)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtncontraseña;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtccontraseña;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ErrorProvider epmal;
    }
}