﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using WMPLib;

namespace Interfaz_Grafica
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        WindowsMediaPlayer wmp;

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        private void btnSlide_Click(object sender, EventArgs e)
        {
            if (MenuVertical.Width == 250)
            {
                MenuVertical.Width = 80;
            }
            else
                MenuVertical.Width = 250;
        }

        private void IconClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void IconMaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            IconRestaurar.Visible = true;
            IconMaximizar.Visible = false;
        }

        private void IconRestaurar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            IconRestaurar.Visible = false;
            IconMaximizar.Visible = true;
        }

        private void IconMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            IconRestaurar.Visible = false;
            try
            {
                if (wmp == null)
                {
                    wmp = new WindowsMediaPlayer();
                    wmp.URL = Application.StartupPath + @"\musicas\TheFatRat_-_Monody_feat._Laura_Brehm_.mp3";
                    wmp.controls.play();
                    wmp.newMedia(Application.StartupPath + @"\musicas\Seico_-_Get_Outta_My_Jungle.mp3");
                    wmp.controls.play();
                }
            }
            catch(Exception ms)
            {
                MessageBox.Show("Error : " + ms);
            }
        }
        
        private void BarraTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle,0x112,0xf012,0);
        }

        private void btnListaAlumnos_Click(object sender, EventArgs e)
        {
            TablaAlumnos frm = new TablaAlumnos();
            RetirarAlumno ra = new RetirarAlumno();
            AgregarAlumnos aa = new AgregarAlumnos();
            ra.Visible = false;
            aa.Visible = false;
            frm.Show();
            frm.Location = new Point(360, 300);
            frm.Left = 600;
            frm.Top = 300;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AgregarAlumnos frm = new AgregarAlumnos();
            TablaAlumnos ta = new TablaAlumnos();
            RetirarAlumno ra = new RetirarAlumno();
            if (frm.Visible == true)
            {
                frm.Close();
            }
            else
            {
                ta.Visible = false;
                ra.Visible = false;
                frm.Show();
                frm.Location = new Point(360, 300);
                frm.Left = 600;
                frm.Top = 300;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RetirarAlumno frm = new RetirarAlumno();
            AgregarAlumnos aa = new AgregarAlumnos();
            TablaAlumnos ta = new TablaAlumnos();
            aa.Visible = false;
            ta.Visible = false;
            frm.Show();
            frm.Location = new Point(360, 300);
            frm.Left = 600;
            frm.Top = 300;
        }
    }
}
