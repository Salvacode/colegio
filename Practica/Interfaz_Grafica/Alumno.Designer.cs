﻿namespace Interfaz_Grafica
{
    partial class Alumno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Alumno));
            this.lblbienvenido = new System.Windows.Forms.Label();
            this.dgnotas = new System.Windows.Forms.DataGridView();
            this.btnVernotas = new System.Windows.Forms.Button();
            this.btnpromediar = new System.Windows.Forms.Button();
            this.lblnota = new System.Windows.Forms.Label();
            this.lblresultado = new System.Windows.Forms.Label();
            this.IconClose = new System.Windows.Forms.PictureBox();
            this.IconMinimizar = new System.Windows.Forms.PictureBox();
            this.lblcodigo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgnotas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IconClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IconMinimizar)).BeginInit();
            this.SuspendLayout();
            // 
            // lblbienvenido
            // 
            this.lblbienvenido.AutoSize = true;
            this.lblbienvenido.Font = new System.Drawing.Font("News706 BT", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblbienvenido.Location = new System.Drawing.Point(204, 23);
            this.lblbienvenido.Name = "lblbienvenido";
            this.lblbienvenido.Size = new System.Drawing.Size(165, 32);
            this.lblbienvenido.TabIndex = 0;
            this.lblbienvenido.Text = "Bienvenido";
            // 
            // dgnotas
            // 
            this.dgnotas.BackgroundColor = System.Drawing.Color.DodgerBlue;
            this.dgnotas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgnotas.Location = new System.Drawing.Point(22, 85);
            this.dgnotas.Name = "dgnotas";
            this.dgnotas.Size = new System.Drawing.Size(762, 77);
            this.dgnotas.TabIndex = 2;
            // 
            // btnVernotas
            // 
            this.btnVernotas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(150)))), ((int)(((byte)(255)))));
            this.btnVernotas.FlatAppearance.BorderSize = 0;
            this.btnVernotas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(170)))), ((int)(((byte)(255)))));
            this.btnVernotas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVernotas.Font = new System.Drawing.Font("Comic Sans MS", 11.5F);
            this.btnVernotas.ForeColor = System.Drawing.Color.White;
            this.btnVernotas.Image = global::Interfaz_Grafica.Properties.Resources.notas;
            this.btnVernotas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVernotas.Location = new System.Drawing.Point(90, 182);
            this.btnVernotas.Name = "btnVernotas";
            this.btnVernotas.Size = new System.Drawing.Size(141, 55);
            this.btnVernotas.TabIndex = 5;
            this.btnVernotas.Text = "Ver Notas";
            this.btnVernotas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnVernotas.UseVisualStyleBackColor = false;
            this.btnVernotas.Click += new System.EventHandler(this.btnVernotas_Click);
            // 
            // btnpromediar
            // 
            this.btnpromediar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(150)))), ((int)(((byte)(255)))));
            this.btnpromediar.FlatAppearance.BorderSize = 0;
            this.btnpromediar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(170)))), ((int)(((byte)(255)))));
            this.btnpromediar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpromediar.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnpromediar.ForeColor = System.Drawing.Color.White;
            this.btnpromediar.Image = global::Interfaz_Grafica.Properties.Resources.calcu;
            this.btnpromediar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnpromediar.Location = new System.Drawing.Point(470, 173);
            this.btnpromediar.Name = "btnpromediar";
            this.btnpromediar.Size = new System.Drawing.Size(141, 64);
            this.btnpromediar.TabIndex = 6;
            this.btnpromediar.Text = "Promediar";
            this.btnpromediar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnpromediar.UseVisualStyleBackColor = false;
            this.btnpromediar.Click += new System.EventHandler(this.btnpromediar_Click);
            // 
            // lblnota
            // 
            this.lblnota.AutoSize = true;
            this.lblnota.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblnota.Location = new System.Drawing.Point(651, 195);
            this.lblnota.Name = "lblnota";
            this.lblnota.Size = new System.Drawing.Size(76, 25);
            this.lblnota.TabIndex = 7;
            this.lblnota.Text = "label3";
            // 
            // lblresultado
            // 
            this.lblresultado.AutoSize = true;
            this.lblresultado.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblresultado.Location = new System.Drawing.Point(249, 283);
            this.lblresultado.Name = "lblresultado";
            this.lblresultado.Size = new System.Drawing.Size(76, 25);
            this.lblresultado.TabIndex = 8;
            this.lblresultado.Text = "label4";
            // 
            // IconClose
            // 
            this.IconClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IconClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IconClose.Image = ((System.Drawing.Image)(resources.GetObject("IconClose.Image")));
            this.IconClose.Location = new System.Drawing.Point(764, 3);
            this.IconClose.Name = "IconClose";
            this.IconClose.Size = new System.Drawing.Size(30, 30);
            this.IconClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.IconClose.TabIndex = 9;
            this.IconClose.TabStop = false;
            this.IconClose.Click += new System.EventHandler(this.IconClose_Click);
            // 
            // IconMinimizar
            // 
            this.IconMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.IconMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IconMinimizar.Image = global::Interfaz_Grafica.Properties.Resources.icon_minimizar;
            this.IconMinimizar.Location = new System.Drawing.Point(728, 3);
            this.IconMinimizar.Name = "IconMinimizar";
            this.IconMinimizar.Size = new System.Drawing.Size(30, 30);
            this.IconMinimizar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.IconMinimizar.TabIndex = 10;
            this.IconMinimizar.TabStop = false;
            this.IconMinimizar.Click += new System.EventHandler(this.IconMinimizar_Click);
            // 
            // lblcodigo
            // 
            this.lblcodigo.AutoSize = true;
            this.lblcodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblcodigo.Location = new System.Drawing.Point(249, 195);
            this.lblcodigo.Name = "lblcodigo";
            this.lblcodigo.Size = new System.Drawing.Size(86, 25);
            this.lblcodigo.TabIndex = 11;
            this.lblcodigo.Text = "Codigo";
            // 
            // Alumno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 350);
            this.Controls.Add(this.lblcodigo);
            this.Controls.Add(this.IconMinimizar);
            this.Controls.Add(this.IconClose);
            this.Controls.Add(this.lblresultado);
            this.Controls.Add(this.lblnota);
            this.Controls.Add(this.btnpromediar);
            this.Controls.Add(this.btnVernotas);
            this.Controls.Add(this.dgnotas);
            this.Controls.Add(this.lblbienvenido);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Alumno";
            this.Text = "Alumno";
            this.Load += new System.EventHandler(this.Alumno_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgnotas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IconClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IconMinimizar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblbienvenido;
        private System.Windows.Forms.DataGridView dgnotas;
        private System.Windows.Forms.Button btnVernotas;
        private System.Windows.Forms.Button btnpromediar;
        private System.Windows.Forms.Label lblnota;
        private System.Windows.Forms.Label lblresultado;
        private System.Windows.Forms.PictureBox IconClose;
        private System.Windows.Forms.PictureBox IconMinimizar;
        private System.Windows.Forms.Label lblcodigo;
    }
}