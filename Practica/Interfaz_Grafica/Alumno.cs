﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Interfaz_Grafica
{
    public partial class Alumno : Form
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-H7CMJ6I\\SAMUEL;Initial Catalog=Colegio_Santa_Khalifa;User ID=salva;Password=7741148");

        int N1 = 0, N2 = 0, N3 = 0, N4 = 0, N5 = 0, N6 = 0, N7 = 0;
        public string user;
        public Alumno()
        {
            InitializeComponent();
        }

        private void IconClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnpromediar_Click(object sender, EventArgs e)
        {
            Promediar(lblcodigo.Text);

   
        }

        private void IconMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        public DataTable Notas(string codi)
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter("SP_NOTAS", con);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            da.SelectCommand.Parameters.AddWithValue("COD", codi);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        private void Bienvenido(string dato)
        {
          
            con.Open();
            SqlCommand cmd = new SqlCommand("SP_BIENVENIDO", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("USER", dato);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if(dt.Rows.Count == 1){
                lblbienvenido.Text = "Bienvenido " + dt.Rows[0][0].ToString();
                lblcodigo.Text = dt.Rows[0][1].ToString();
            }
            con.Close();
        }


        private void Promediar(string dato)
        {
            con.Close();
            con.Open();
            SqlCommand cmd = new SqlCommand("SP_NOTAS", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("COD", dato);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count == 1)
            {
                N1 = Convert.ToInt32(dt.Rows[0][0].ToString());
                N2 = Convert.ToInt32(dt.Rows[0][1].ToString());
                N3 = Convert.ToInt32(dt.Rows[0][2].ToString());
                N4 = Convert.ToInt32(dt.Rows[0][3].ToString());
                N5 = Convert.ToInt32(dt.Rows[0][4].ToString());
                N6 = Convert.ToInt32(dt.Rows[0][5].ToString());
                N7 = Convert.ToInt32(dt.Rows[0][6].ToString());

                int promedio = (N1 + N2 + N3 + N4 + N5 + N6 + N7) / 7;
                if(promedio <= 10)
                {
                    lblresultado.Text = "Usted reprobo el bimestre ";
                }
                else
                {
                    lblresultado.Text = "Felicidades, ustede aprobo el bimestre";
                }

                lblnota.Text = Convert.ToString(promedio);
              
            }
            con.Close();
        }

        private void Alumno_Load(object sender, EventArgs e)
        {
           Bienvenido(user);
        }

        private void btnVernotas_Click(object sender, EventArgs e)
        {
            dgnotas.DataSource = Notas(lblcodigo.Text);
        }
    }
}